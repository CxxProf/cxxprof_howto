from conans import ConanFile, CMake

class CxxProfConan(ConanFile):
    name = "cxxprof_howto"
    version = "1.0.0"
    url = "https://gitlab.com/groups/CxxProf"
    license = "GNU LESSER GENERAL PUBLIC LICENSE Version 3"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake", "virtualrunenv"
    exports = "*"

    def requirements(self):
        self.requires.add("cxxprof_simple/1.0.0@nils/testing")
        
    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*.exe", dst="bin", src="bin")
