
#include <chrono>
#include <functional>
#include <iostream>
#include <memory>
#include <thread>

#include <cxxprof_simple/CxxProf.h>

//This is for the Threading-Test, more then 5 seem to clutter the resulting data
const unsigned int NUM_THREADS = 5;

/**
 * This is simply a method which takes a lot of time and therefore should
 * produce a very long activity
 */
void longOperation(int givenTime)
{
    CXXPROF_ACTIVITY("longOperation");
    std::this_thread::sleep_for(std::chrono::milliseconds(givenTime));
}

int recursiveOperation(int someValue)
{
    CXXPROF_ACTIVITY("recursiveOperation");

    //Adding PlotValues helps us to see how the application is handling the change of values over time
    CXXPROF_PLOT("SomeValue", someValue);

    if (someValue == 3)
    {
        //Let's add some complexity by sleeping a bit when someValue reaches 3
        CXXPROF_ACTIVITY("recursiveOperation::middle");
        longOperation(200);
    }
    if(someValue < 5) //do recursion until the value hits 5
    {
        //just sleep a bit to simulate something costly going on here...
        longOperation(50);
        someValue = recursiveOperation(++someValue);
    }

    return someValue;
}

int main()
{
    //Initialize CxxProf first
    CXXPROF_INIT();
    CXXPROF_PROCESS_ALIAS("cxxprof_howto");
    CXXPROF_THREAD_ALIAS("MainThread");

    //Measure how long the rest of this main() takes
    CXXPROF_ACTIVITY("main");
    
    //Add a mark for a better overview of which state the application is currently in
    CXXPROF_MARK("LongOperation start");
    longOperation(1000);
    longOperation(800);

    //Start some LoopOperations in extra Threads
    CXXPROF_MARK("Threading start");
    std::vector< std::shared_ptr<std::thread> > threads;
    for (unsigned int index = 0; index < NUM_THREADS; ++index)
    {
        //create a new thread which waits 100 millisecs
        //NOTE: Do not set a Thread Alias here. Brofiler will generate something automatically
        std::shared_ptr<std::thread> newThread( new std::thread(std::bind(longOperation, 100)) );
        threads.push_back(newThread);
    }

    //As long as the threads are running let's do the recursive test
    //This should result in some activities which are stacked upon each other
    CXXPROF_MARK("RecursiveOperation start");
    recursiveOperation(1);

    //This test conditional statements
    CXXPROF_MARK_COND("Conditional Test Start - true", true);
    CXXPROF_MARK_COND("Conditional Test Start - false", false);
    {
        CXXPROF_ACTIVITY_COND("ActCondTest - true", true);
        CXXPROF_ACTIVITY_COND("ActCondTest - false", false);
        CXXPROF_PLOT_COND("PlotCondTest - true", 1.0, true);
        CXXPROF_PLOT_COND("PlotCondTest - false", 1.0, false);
    }

    //Now let's wait until the loopThreads are finished
    std::vector<std::shared_ptr<std::thread> >::iterator threadIter = threads.begin();
    for (; threadIter != threads.end(); ++threadIter)
    {
        (*threadIter)->join();
    }

    //Here it is demonstrated that we can also create multiple Activities in the same scope
    CXXPROF_MARK("ScopeTest start");
    {
        CXXPROF_ACTIVITY("scopeTest");
        CXXPROF_ACTIVITY("scopeTest");
    }

    //Shutdown the CxxProf cleanly
    //This ends the 'main' activity and everything else which is still running (probably the scopeTest)
    //It also takes care of sending the data which hasn't been sent yet
    CXXPROF_SHUTDOWN();

    return 0;
}
